// Las constantes se pueden declarar fuera de la
// funcion o dentro de las funciones
const SPACES_CAD: u8 = 23;
fn main() {
    // las variables solo dentro de las funciones
    let edad_maxima: u8 = 99;
    // let edad_maxima = 12;
    println!("Los espacios entre espacion son: {}", SPACES_CAD);
    println!("La edad maxima de una persona es de {} años", edad_maxima);
}
