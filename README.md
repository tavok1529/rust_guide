# Study guide to learn Rust

## How to install Rust
To install see the Documentation.

1. to linux [URL](https://www.rust-lang.org/learn/get-started)
2. to other SO look at the previus URL


### Commands to use in Rust

When Rust is installed we can use the next commands, and you can see the commands [here](https://doc.rust-lang.org/cargo/commands/cargo-run.html).

1. **rustup update** ( to update Rust)
2. **rustc file_code.rs** (to compile a single file).

**cargo** es el package manager de Rust, este nos ayuda a crear un proyecto en Rust ( equivalente a maven, gradle )-

1. **cargo --version**  ( para obtener la version de cargo)
2. **cargo new hello_world** ( genera una estructura de proyecto)
3. **cargo check** : Check a local package and all of its dependencies for errors. This will essentially compile the packages without performing the final step of code generation, which is faster than running cargo build. The compiler will save metadata files to disk so that future runs will reuse them if the source has not been modified. Some diagnostics and errors are only emitted during code generation, so they inherently won't be reported with cargo check.
4. **cargo run** : Run a binary or example of the local package.All the arguments following the two dashes (--) are passed to the binary to run. If you're passing arguments to both Cargo and the binary, the ones after -- go to the binary, the ones before go to Cargo.
5. **cargo build** : Compile local packages and all of their dependencies.
6. **cargo build --release** : Build optimized artifacts with the release profile. See the PROFILES section for details on how this affects profile selection.
 

## Cargo.lock
---
If you’re building a non-end product, such as a rust library that other rust packages will depend on, put Cargo.lock in your .gitignore. If you’re building an end product, which are executable like command-line tool or an application, or a system library with crate-type of staticlib or cdylib, check Cargo.lock into git

# Crates.io
[Page](https://crates.io/) to view all packages to Rust.
